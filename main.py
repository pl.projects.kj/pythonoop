class Animal:
    def __init__(self, name):
        self.name = name
        self.levelOfFood = 100

    def makeASound(self):
        pass

    def getAName(self):
        return self.name

    def feed(self, foodName):
        pass


class Cat(Animal):
    def __init__(self, name, race):
        super().__init__(name)
        self.race = race

    def makeASound(self):
        return "MIAAAAAAAAAAAAAAAAAAAAAAAAU"

    def feed(self, foodName):

        if foodName == "Cat food":
            if self.levelOfFood + 20 <= 150:
                self.levelOfFood += 20

            print("MRRR, PSZNE JEDZONKO!")
        elif foodName == "Fish food":
            self.levelOfFood -= 10
            print("Bleeeee, brzuszek boli :C")


class Fish(Animal):
    def __init__(self, name, colour):
        super().__init__(name)
        self.colour = colour

    def makeASound(self):
        return "GULLLLL, GULLLLL"

    def feed(self, foodName):
        if foodName == "Fish food":
            if self.levelOfFood + 20 <= 140:
                self.levelOfFood += 20

            print("MRRR, PSZNE JEDZONKO!")
        elif foodName == "Fish food":
            self.levelOfFood += 10
            print("Bleeeee, brzuszek boli :C")


koteczek = Cat("Oluś", "persian")

koteczekDwa = Cat("Miłko", "bengal")

rybeczka = Fish("Miu", "blue")

rybeczkaDwa = Fish("Fiu", "red")

animals = [koteczek, koteczekDwa, rybeczka, rybeczkaDwa]
# print(animals)

rybeczka.feed("Fish food")
rybeczka.feed("Fish food")
rybeczka.feed("Fish food")
rybeczka.feed("Fish food")

print(rybeczka.levelOfFood)
